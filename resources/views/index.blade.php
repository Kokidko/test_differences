<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <form action="/check" method="post">
        {{csrf_field()}}


        <textarea name="firstField" id="firstField" cols="30" rows="10" required></textarea>
        <textarea name="secondField" id="secondField" cols="30" rows="10" required></textarea>
        <br>
        <button type="submit">Check</button>
    </form>

</body>
</html>