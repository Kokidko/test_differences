<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CheckController extends Controller
{
    public function index() {
        return view('index');
    }

    public function check() {

        $firstField = request()->post('firstField');
        $secondField = request()->post('secondField');

        $firstArray = preg_split('/\r\n|[\r\n]/', $firstField);
        $secondArray = preg_split('/\r\n|[\r\n]/', $secondField);

        foreach ($firstArray as $key => $firstElement) {
            if(isset($secondArray[$key])) {
                if($firstElement == $secondArray[$key]) {
                    $results[] = [$key+1, ' ', $firstElement];
                } elseif($firstElement != $secondArray[$key]) {
                    $results[] = [$key+1, '*', $firstElement . '|' . $secondArray[$key]];
                }
            } else {
                $results[] = [$key+1, '-', $firstElement];
            }
        }

        $countFirst = count($firstArray);
        $countSecond = count($secondArray);

        if($countFirst < $countSecond) {
            $countResults = count($results);
            for($i = $countResults; $i < $countSecond; $i++) {
                $results[] = [$i+1, '+', $secondArray[$i]];
            }
        }

        return view('check')->with(compact('results'));
    }
}
